from django.apps import AppConfig


class ThroughTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'through_test'
