from django.shortcuts import render
from django.utils import timezone
import datetime

def index(request):
    context = {
        "date1" : timezone.now(),
        "date2" : timezone.now() + datetime.timedelta(days=400)
    }
    #hi
    return render(request, 'through_test/index.html', context)

