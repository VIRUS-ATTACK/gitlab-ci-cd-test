import humanize
from django import template
register = template.Library()

@register.simple_tag
def get_url(date1, date2):
    return humanize.precisedelta(date2 - date1, minimum_unit="hours")