from django.db import models
#from django.contrib.postgres.fields import ArrayField
# Create your models here.
from django.db import models

class Dummy(models.Model):
    desc = models.CharField(max_length=20)

    def __str__(self):
        return self.desc

class Person(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

class Group(models.Model):
    name = models.CharField(max_length=128)
    members = models.ManyToManyField(Person, through='Membership')
    dummies  = models.ManyToManyField(Dummy, related_name="group_dummy")

    def __str__(self):
        return self.name

class Membership(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

class Issue(models.Model):
    name = models.CharField(max_length=100)
    assigned_people = models.CharField(max_length=2000)
    assigned_departments = models.CharField(max_length=2000)