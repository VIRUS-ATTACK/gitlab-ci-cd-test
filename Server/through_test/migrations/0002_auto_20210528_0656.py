# Generated by Django 3.2.3 on 2021-05-28 06:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('through_test', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='membership',
            name='date_joined',
        ),
        migrations.RemoveField(
            model_name='membership',
            name='invite_reason',
        ),
    ]
